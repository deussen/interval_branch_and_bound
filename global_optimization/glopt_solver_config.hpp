#pragma once
#include <vector>
#include <cfloat>
#include <limits>
#include <string>
#include <sstream>
using namespace std;

template<typename interval, typename float_type>
class solver_config {
  bitset<3> _print_mode = 0;
  size_t _ntime = 1;
  size_t _print_status_every=100000000;

  float_type _epsx = sqrt(numeric_limits<float_type>::epsilon());
  float_type _epsy = sqrt(numeric_limits<float_type>::epsilon());
  float_type _epsns = 1e-4;

  vector<interval> _domain;
  vector<float_type> _param;

  void read_argv(int argc, char* argv[]) {
    size_t cnt=1;
    if (argc >= cnt+1) _print_mode = stoi(argv[cnt]);
    cnt++;
    if (argc >= cnt+1) _epsx = stod(argv[cnt])<0?sqrt(numeric_limits<float_type>::epsilon()):stod(argv[cnt]);
    cnt++;
    if (argc >= cnt+1) _epsy = stod(argv[cnt])<0?sqrt(numeric_limits<float_type>::epsilon()):stod(argv[cnt]);
    cnt++;
    if (argc >= cnt+1) _epsns = stod(argv[cnt])<0?sqrt(numeric_limits<float_type>::epsilon()):stod(argv[cnt]);
    cnt++;
  }

public:
  solver_config(int argc, char* argv[], vector<interval>& domain, vector<float_type>& param) : _domain(domain), _param(param) {
    read_argv(argc,argv);
  }

  solver_config(int argc, char* argv[], vector<interval>& domain) : _domain(domain), _param() {
    read_argv(argc,argv);
  }

  void print() {
    cout << " PRINT_MODE = " << _print_mode.test(0) << endl;
    cout << " PRINT_NOGL_MODE = " << _print_mode.test(1) << endl;
    cout << " PRINT_FAIL_MODE = " << _print_mode.test(2) << endl;
    cout << " N = " << _domain.size() << endl;
    cout << " EPS_X = " << _epsx << endl;
    cout << " EPS_Y = " << _epsy << endl;
    cout << " EPS_NS = " << _epsns << endl;
#pragma omp parallel
#pragma omp single
    {
      cout << " THREADS = " << omp_get_num_threads() << endl;
    }
  }

  const bool print_mode() { return _print_mode.test(0); }
  const bool print_mode_ns() { return _print_mode.test(1); }
  const bool print_mode_fail() { return _print_mode.test(2); }
  const size_t& print_status_every() { return _print_status_every; }
  const float_type& eps_x() { return _epsx; }
  const float_type& eps_y() { return _epsy; }
  const float_type& eps_ns() { return _epsns; }
  const vector<interval>& domain() { return _domain; }
  const vector<float_type>& param() { return _param; }
  void set_domain(vector<interval>& domain) { _domain = domain; }
  void set_param(vector<float_type>& param) { _param = param; }
};

template<typename float_type>
class solver_status {
  unsigned long long _master_cnt;
  vector<unsigned long long> _counter;
  float_type _global_min_bound;
public:
  solver_status() : _counter(vector<unsigned long long>(5,0)), _master_cnt(0) {
    _global_min_bound = numeric_limits<float_type>::max();
  }
  void print() {
    stringstream stream;
    stream << _counter[0] << " MIN=" << _global_min_bound;
    stream << "\t";
    stream << " V=" << _counter[1];
    stream << " NS=" << _counter[2];
    stream << " OPT=" << _counter[3];
    stream << " R=" << _counter[4];
    stream << endl;
    cout << stream.rdbuf();
  }
  void print_final() {
    print();
    stringstream stream;
    stream << "global minimum bound is " << _global_min_bound << endl;
    stream << "total         \t" << _counter[0] << endl;
    stream << "refined       \t" << _counter[4] << "\t" << _counter[4]/static_cast<float>(_counter[0]) << endl;
    stream << "non smooth    \t" << _counter[2] << "\t" << _counter[2]/static_cast<float>(_counter[0]) << endl;
    stream << "not converged \t" << _counter[3] << "\t" << _counter[3]/static_cast<float>(_counter[0]) << endl;
    cout << stream.rdbuf();
  }
  unsigned long long& master_cnt() { return _master_cnt; }
  vector<unsigned long long>& counter() { return _counter; }
  unsigned long long& counter(size_t i) { return _counter[i]; }
  float_type& global_min_bound() { return _global_min_bound; }
};

struct problem_t {
  bool min=true;
};


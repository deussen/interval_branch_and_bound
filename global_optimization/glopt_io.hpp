#pragma once
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include "omp.h"
#include "glopt_solver_config.hpp"
using namespace std;

template<typename T>
inline void print_scenario_file(int result, const vector<T>& x, const T& y, string str="opt") {
  stringstream stream;
  stream << setprecision(7);
  stream << result;
  for (auto const& xi : x) {
    stream << " " << xi.lower() << " " << xi.upper();
  }
  stream << ":";
  stream << " " << y.lower() << " " << y.upper();
  stream << "\n";
  stringstream filename;
  filename << str << omp_get_thread_num() << ".txt";
  std::fstream file(filename.str(), std::fstream::out | std::fstream::app);
  file << stream.rdbuf();
  file.close();
}

template<typename T>
inline void print_scenario_file_fail(const vector<T>& x, string str="fail") {
  size_t result = 0;
  stringstream stream;
  stream << setprecision(7);
  stream << result;
  for (auto const& xi : x) {
    stream << " " << xi.lower() << " " << xi.upper();
  }
  stream << "\n";
  stringstream filename;
  filename << str << omp_get_thread_num() << ".txt";
  std::fstream file(filename.str(), std::fstream::out | std::fstream::app);
  file << stream.rdbuf();
  file.close();
}

# Getting the input parameters
x1l = ARG1
x1r = ARG2
x2l = ARG3
x2r = ARG4
fail = ARG5

# Initialize an object number
if (!exists("object_number")) object_number = 1;

# Set the loudspeaker at the given position and rotate it by a rotation matrix:
set obj object_number rect from x1l,x2l to x1r,x2r

# Set the color etc.
if (fail==0) set object object_number front lw 0 fs noborder pattern 3 fc rgb "#e41a1c";\
  else if (fail==1)      set object object_number front fs pattern 3 fc rgb "#4daf4a";\
  else if (fail==2)      set object object_number front fs pattern 3 fc rgb "#ff7f00";\
  else if (fail==3)      set object object_number front fs pattern 3 fc rgb "#377eb8";\
  else if (fail==4)      set object object_number front fs pattern 3 fc rgb "#ff7f00";\
  else if (fail==5)      set object object_number front fs pattern 3 fc rgb "#984ea3";\
  else if (fail==6)      set object object_number front fs pattern 3 fc rgb "#ff7f00";\
  else if (fail==7)      set object object_number front fs pattern 3 fc rgb "#377eb8";\
  else if (fail==8)      set object object_number front fs pattern 3 fc rgb "#ff7f00";\
  else       set object object_number front fs pattern 3 fc rgb "#e41a1c";\
 

# Count the object number
object_number = object_number+1

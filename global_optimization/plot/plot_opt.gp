#! /usr/bin/gnuplot

reset
set term pdf enhanced color font "Times, 12" size 18cm,18cm
set output "opt.pdf"
set multiplot
set xlabel "x[0]" font "Times-Italic,12"
set ylabel "x[1]" font "Times-Italic,12"
set xrange[-3:3]
set yrange[-3:3]
unset key
set size ratio -1

set autoscale fix
plot "six_hump_iso.png" binary filetype=png origin=(-3,-3) dx=0.004 dy=0.004 w rgbimage 

set table '/dev/null'
add_rect(x1l,x1r,x2l,x2r,fail) = sprintf(\
    'call "set_rect.gp" "%f" "%f" "%f" "%f" "%f" "%f";',x1l,x1r,x2l,x2r,fail)
CMD = ''
plot 'glmin.txt' u 1:(CMD = CMD.add_rect($2,$3,$4,$5,$1)),\
     'noglmin.txt' u 1:(CMD = CMD.add_rect($2,$3,$4,$5,$1)),\
		 'fail.txt' u 1:(CMD = CMD.add_rect($2,$3,$4,$5,$1))
eval(CMD)
unset table

plot -1000000

quit

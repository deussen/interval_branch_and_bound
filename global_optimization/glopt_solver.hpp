#pragma once
#ifndef DCO_NO_ZERO_EDGE_CHECK
  #define DCO_NO_ZERO_EDGE_CHECK
#endif
#ifdef MCPP_ADJM
  #include "interoperability_dco_mcpp.hpp"
#endif
#include <fenv.h>
#include "boost/numeric/interval.hpp"
#include "boost/numeric/interval/io.hpp"
#include <vector>
#include <bitset>
#include "omp.h"
#include "glopt_solver_config.hpp"
#include "glopt_io.hpp"
#include "objective.hpp"

using namespace std;

template<typename float_type, typename interval>
class glopt_solver {
  solver_config<interval,float_type> *_solver_config;
  solver_status<float_type> *_solver_status;
  typedef typename interval::traits_type::checking checking;
public:
  glopt_solver(int argc, char* argv[]) : _solver_config(new solver_config<interval,float_type>(argc,argv)), _solver_status(new solver_status<float_type>()) {
    feenableexcept(FE_INVALID);
    _solver_config->print();
  }
  glopt_solver(int argc, char* argv[],vector<interval> domain) : _solver_config(new solver_config<interval,float_type>(argc,argv,domain)), _solver_status(new solver_status<float_type>()) {}
  //void configure(int argc, char* argv[]);
  void run() {
    _solver_config->print();
#pragma omp parallel
    {
#pragma omp single
      {
        push_task({true},_solver_config->domain());
#pragma omp taskwait
      }
    }
    _solver_status->print_final();
  }

private:
  template <typename T>
  void value(const problem_t &P, const vector<T> &x, T &y) {
    if (P.min)
      y = objective(x);
    else
      y = -objective(x);
  }

  // Generate new instaces of problem P with subdomains of x only splitting those that are marked in the x_mask
  // Return if any instance was generated
  // TODOs: priority for OMP task
  bool branching(const problem_t &P, const vector<interval> &x, const vector<bool> &x_mask) {
    bool task_generated = false;
    size_t n_split = 0;
    vector<interval> new_domain(x);
    vector< vector<interval> > split_space(x.size(),vector<interval>(2));
    for (size_t i=0; i<x.size(); i++) {
      if (x_mask[i]) {
        n_split++;
        split_space[i][0].set(x[i].lower(),median(x[i]));
        split_space[i][1].set(median(x[i]),x[i].upper());
      } else {
        split_space[i].resize(1);
        new_domain[i] = x[i];
      }
    }
    assert(n_split <= x.size());
    if (n_split > 0) {
      task_generated = true;
      for (size_t c=0; c<pow(2,n_split); c++) {
        bitset<64> split_comb(c);
        size_t i_split = 0;
        for(size_t i=0; i<x.size(); i++) {
          if (x_mask[i]) {
            new_domain[i] = split_space[i][split_comb[i_split]];
            i_split++;
          }
        }
        push_task(P,new_domain);
      }
    }
    if (task_generated)
      increment_counter(4);
    return task_generated;
  }

  // Computes function value with interval type and mark problem for elimination
  // if it can't contain global minimum
  // Function evaluations in real arithemtic to find new global minimum
  // Update global minimum
  void value_check(bool &discard, const problem_t &P, const vector<interval> &x, interval &y) {
    value(P,x,y);
    set_nan_to_inf(y);
    if (y.lower() > _solver_status->global_min_bound()) {
      increment_counter(1);
      discard = true;
      return;
    } else {
      float_type y_value_check = numeric_limits<float_type>::max();
      vector<float_type> x_mid(x.size());
      for (size_t i=0; i<x.size(); i++)
        x_mid[i] = median(x[i]);
      value(P,x_mid,y_value_check);
      if (y_value_check < _solver_status->global_min_bound())
        set_opt(y_value_check);
    }
  }

  inline void push_task(const problem_t P, const vector<interval> &x) {
#pragma omp task firstprivate(x)
    { task(P,x); }
  }

  inline void increment_counter(size_t i) {
#pragma omp atomic
    _solver_status->counter(i)++;
  }

  inline void set_opt(const float_type &y_opt_new) {
#pragma omp critical
    {
      if (y_opt_new < _solver_status->global_min_bound())
        _solver_status->global_min_bound() = y_opt_new;
    }
  }

  inline void set_nan_to_inf(interval &v) {
    if (checking::is_nan(v.lower())) v.set(checking::neg_inf(), v.upper());
    if (checking::is_nan(v.upper())) v.set(v.lower(), checking::pos_inf());
  }

  // If an exception is thrown e.g. due to undefined comparisons in interval arithemtic
  // domain is refined
  void exception(const problem_t &P, const vector<interval> &x, const vector<bool> &x_mask) {
    for (size_t i=0; i<x.size(); i++) {
      if (x_mask[i] && width(x[i]) > _solver_config->eps_ns()) {
        bool task_generated = branching(P,x,x_mask);
        if (task_generated) return;
        break;
      }
    }
    increment_counter(2);
    if (_solver_config->print_mode_fail())
      print_scenario_file_fail(x);
  }

  // Task is defined by (sub-)domain and index of objective
  // Computation of objective, gradient of obejctive and Hessian vector product
  // in interval arithemtic
  // Finding of separator to decompose problem
  //
  // Elimination of domain that cannot contain global minimum
  // or refinement of domain if it is not fine enough
  void task(const problem_t &P, const vector<interval>& x) {
    increment_counter(0);
#pragma omp master
    {
      _solver_status->master_cnt()++;
      if (_solver_status->master_cnt()%(_solver_config->print_status_every()/64)==0)
        _solver_status->print();
    }
    vector<bool> x_mask(x.size(), true);
    bool discard = false;
    interval y;

    for (size_t i=0; i<x.size(); i++)
      if (width(x[i]) <= _solver_config->eps_x())
        x_mask[i] = false;

    try {
      value_check(discard,P,x,y);
      // Check if the value range can contain a global minimum
      if (!_solver_config->print_mode_ns() && discard) return;
    } catch (boost::numeric::interval_lib::comparison_error&) {
      exception(P,x,x_mask);
    } catch (std::exception& e) {
      assert(false);
    }

    if (discard) {
      if (_solver_config->print_mode_ns()) {
        print_scenario_file(discard+1,x,y,"noglmin");
      }
      return;
    }
    assert(!discard);

    bool task_generated = branching(P,x,x_mask);
    if (!task_generated) {
      increment_counter(3);
      if (_solver_config->print_mode())
        print_scenario_file(1,x,y,"glmin");
    }
  }
};

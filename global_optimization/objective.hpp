#pragma once
#include <vector>
#include <cmath>

#define N 2
#ifndef N
#define N 2
#endif

template<typename T>
void set_problem_params(vector<T>& domain) {
  size_t n = 2;
  domain.resize(n,T(-3.0,4.0));
}

template<typename T>
T objective(const vector<T> &x) {
  T y = T(0.0);
  for (size_t i=0; i<x.size(); i++) {
    y += x[i]*x[i];
  }
  return y;
}

#include <iostream>
#include <vector>
#define DCO_DEBUG
#include "glopt_solver.hpp"

template <typename T>
  using checking = boost::numeric::interval_lib::checking_base<T>;

template <typename T>
  using rounding = boost::numeric::interval_lib::rounded_transc_std<T>;

template <typename T>
  using boost_interval_transc_t = boost::numeric::interval
  <T,
   boost::numeric::interval_lib::policies<
     boost::numeric::interval_lib::save_state<rounding<T>>,
     checking<T>
   >
  >;

using namespace std;

//****************************** main ******************************//
int main(int argc, char* argv[]) {
  using interval = boost_interval_transc_t<double>;
  cout.precision(15);
  vector<interval> domain;
  set_problem_params(domain);
  glopt_solver<double,interval> *solver = new glopt_solver<double,interval>(argc,argv,domain);
  solver->run();
//  delete solver;
  return 0;
}
